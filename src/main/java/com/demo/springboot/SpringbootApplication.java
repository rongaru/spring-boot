package com.demo.springboot;

import com.demo.springboot.Utility.DatabaseInitializationUtility;
import com.demo.springboot.repository.AdminRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringbootApplication {

    public static void main( String[] args ) {

        SpringApplication.run( SpringbootApplication.class, args );
    }





    @Bean
    ApplicationRunner applicationRunner( AdminRepository adminRepository ) {

        return args -> {
            adminRepository.save( DatabaseInitializationUtility.getDefaultUser( ) );
        };
    }


}

