package com.demo.springboot.Utility;

import com.demo.springboot.dto.AdminDto;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuthenticationUtility {

    public static AdminDto getCurrentLoggedAdminDto( ) {

        return ( AdminDto ) SecurityContextHolder.getContext( )
                                                 .getAuthentication( )
                                                 .getPrincipal( );
    }

}
