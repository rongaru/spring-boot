package com.demo.springboot.Utility;

import com.demo.springboot.constant.Status;
import com.demo.springboot.dto.AdminDto;
import com.demo.springboot.entity.Admin;

import java.util.Date;

public class DatabaseInitializationUtility {

    public static Admin getDefaultUser( ) {

        Admin user = new Admin( );
        user.setAddress( "Bhaktapur" );
        user.setCreatedByAdmin( getDefaultCreatedByUser( ) );
        user.setCreatedDate( new Date( ) );
        user.setDateOfBirth( new Date( ) );
        user.setEmail( "rongaru@gmail.com" );
        user.setFirstName( "Roshan" );
        user.setGender( "Male" );
        user.setIsAccountNonExpired( true );
        user.setIsAccountNonLocked( true );
        user.setIsCredentialsNonExpired( true );
        user.setIsEnabled( true );
        user.setLastName( "Garu" );
        user.setPassword( Passwords.encode( "user" ) );
        user.setPhone( "9860200899" );
        user.setStatus( Status.CREATE_APPROVED );
        user.setUsername( "user" );
        return user;
    }





    public static Admin getDefaultCreatedByUser( ) {

        Admin user = new Admin( );
        user.setId( 1l );
        return user;
    }





    public static AdminDto getDefaultCreatedByUserDto( ) {

        AdminDto userDto = new AdminDto( );
        userDto.setId( 1l );
        return userDto;
    }

}
