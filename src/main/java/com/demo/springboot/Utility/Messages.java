package com.demo.springboot.Utility;

import com.demo.springboot.constant.MessageSeverity;
import com.demo.springboot.constant.MessageType;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

public class Messages {

    private static void add( String severity, String title, String detail, RedirectAttributes attributes ) {

        attributes.addFlashAttribute( "messageType", severity );
        attributes.addFlashAttribute( "messageTitle", title );
        attributes.addFlashAttribute( "messageDetail", detail );
    }





    public static void addSuccess( String title, String detail, RedirectAttributes attributes ) {

        add( MessageSeverity.SUCCESS, title, detail, attributes );
    }





    public static void addError( String title, String detail, RedirectAttributes attributes ) {

        add( MessageSeverity.ERROR, title, detail, attributes );
    }





    public static void addWarning( String title, String detail, RedirectAttributes attributes ) {

        add( MessageSeverity.WARNING, title, detail, attributes );
    }





    public static void addAlreadyPresent( List< String > properties, RedirectAttributes attributes ) {

        add( MessageSeverity.ERROR, MessageType.PROPERTIES_ALREADY_EXIST, properties.toString( ), attributes );
    }

}
