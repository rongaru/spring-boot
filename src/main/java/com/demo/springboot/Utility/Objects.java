package com.demo.springboot.Utility;

import com.demo.springboot.abstrcats.dto.AbstractEntityHelperDto;
import com.demo.springboot.abstrcats.entity.AbstractEntityHelper;

public class Objects {

    public static boolean isNull( Object o ) {

        return o == null;
    }





    public static boolean isNotNull( Object o ) {

        return o != null;
    }





    public static boolean isEquals( AbstractEntityHelper helper, AbstractEntityHelperDto dto ) {

        return helper != null
                && dto != null
                && helper.getId( )
                         .equals( dto.getId( ) );
    }





    public static boolean isEquals( AbstractEntityHelper helper1, AbstractEntityHelper helper2 ) {

        return helper1 != null
                && helper2 != null
                && helper1.getId( )
                          .equals( helper2.getId( ) );
    }





    public static boolean isEquals( AbstractEntityHelperDto dto1, AbstractEntityHelperDto dto2 ) {

        return dto1 != null
                && dto2 != null
                && dto1.getId( )
                       .equals( dto2.getId( ) );
    }





    public static boolean isNotEquals( AbstractEntityHelper helper, AbstractEntityHelperDto helperDto ) {

        return Booleans.invert( isEquals( helper, helperDto ) );
    }





    public static boolean isNotEquals( AbstractEntityHelper helper1, AbstractEntityHelper helper2 ) {

        return
                Booleans.invert( isEquals( helper1, helper2 ) );
    }





    public static boolean isNotEquals( AbstractEntityHelperDto helperDto1, AbstractEntityHelperDto helperDto2 ) {

        return
                Booleans.invert( isEquals( helperDto1, helperDto2 ) );
    }

}
