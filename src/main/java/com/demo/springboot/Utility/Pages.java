package com.demo.springboot.Utility;

import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Pages {

    public static PageRequest getRequest( Optional< Integer > pageNumber, Optional< Integer > pageSize ) {

        PageRequest pageRequest = PageRequest.of( pageNumber.orElse( 1 ) - 1,
                pageSize.orElse( 10 ) );

        return pageRequest;
    }





    public static List< Integer > getNumbers( Integer totalPages ) {

        List< Integer > pageNumbers = new ArrayList<>( );
        if ( totalPages > 0 ) {
            pageNumbers = IntStream.rangeClosed( 1, totalPages )
                                   .boxed( )
                                   .collect( Collectors.toList( ) );
        }

        return pageNumbers;
    }

}
