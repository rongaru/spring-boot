package com.demo.springboot.Utility;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class Passwords {

    public static PasswordEncoder getPasswordEncoder( ) {

        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder( );
        return passwordEncoder;
    }





    public static String encode( String password ) {

        String encoded = getPasswordEncoder( ).encode( password );
        return encoded;
    }

}
