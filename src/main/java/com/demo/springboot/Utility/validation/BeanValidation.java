package com.demo.springboot.Utility.validation;

import javax.validation.GroupSequence;

@GroupSequence( { NotNullValidation.class, SizeValidation.class, PatternValidation.class } )
public interface BeanValidation {

}
