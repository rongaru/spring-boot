package com.demo.springboot.abstrcats.controller;

import com.demo.springboot.Utility.*;
import com.demo.springboot.Utility.validation.BeanValidation;
import com.demo.springboot.abstrcats.dto.AbstractProfileHelperDto;
import com.demo.springboot.abstrcats.service.AbstractProfileHelperService;
import com.demo.springboot.constant.ControllerUtility;
import com.demo.springboot.constant.MessageType;
import com.demo.springboot.constant.RequestPath;
import com.demo.springboot.constant.Status;
import org.springframework.data.domain.Page;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

public abstract class AbstractController< Dto > {

    protected abstract AbstractProfileHelperService getService( );


    protected abstract Dto getDtoInstance( );


    protected abstract void initCreate( RedirectAttributes attributes );


    protected abstract void initEdit( RedirectAttributes attributes );





    private String getDtoClassName( ) {

        return getDtoInstance( ).getClass( )
                                .getSimpleName( );
    }





    private String getEntityName( ) {

        return getDtoClassName( ).replace( ControllerUtility.DTO, "" );
    }





    private String getDtoVariableName( ) {

        return Strings.lowercaseFirstCharacter( getDtoClassName( ) );
    }





    private String getDtoPageVariableName( ) {

        return getDtoVariableName( ) + Page.class.getSimpleName( );
    }





    private String getPageName( ) {

        return getDtoVariableName( );
    }





    private String getPagePath( ) {

        return RequestPath.ROOT + getDtoClassName( ) + RequestPath.SEPARATOR + RequestPath.PAGE;
    }





    private boolean save( Dto dto ) {

        getHelperDto( dto ).setCreatedByAdminDto( AuthenticationUtility.getCurrentLoggedAdminDto( ) );
        return getService( ).save( dto );
    }





    private boolean update( Dto dto ) {

        getHelperDto( dto ).setUpdatedByAdminDto( AuthenticationUtility.getCurrentLoggedAdminDto( ) );
        return getService( ).update( dto );
    }





    private AbstractProfileHelperDto getHelperDto( Dto dto ) {

        return ( AbstractProfileHelperDto ) dto;
    }





    @GetMapping( RequestPath.PAGE )
    public String page( @RequestParam( ControllerUtility.PAGE_NUMBER ) Optional< Integer > pageNumber,
                        @RequestParam( ControllerUtility.PAGE_SIZE ) Optional< Integer > pageSize,
                        Model model ) {

        Page< Dto > dtoPage = getService( ).getByStatusNotIn( Status.getDeleteStatuses( ),
                Pages.getRequest( pageNumber, pageSize ) );

        model.addAttribute( getDtoPageVariableName( ), dtoPage );
        model.addAttribute( ControllerUtility.PAGE_NUMBERS, Pages.getNumbers( dtoPage.getTotalPages( ) ) );

        return getPageName( );
    }





    @GetMapping( RequestPath.CREATE )
    public String create( RedirectAttributes attributes ) {

        attributes.addFlashAttribute( ControllerUtility.SHOW_CREATE_EDIT_PANEL, true );
        attributes.addFlashAttribute( getDtoVariableName( ), getDtoInstance( ) );

        initCreate( attributes );

        return Requests.redirectTo( getPagePath( ) );
    }





    @GetMapping( RequestPath.EDIT )
    public String edit( @PathVariable( ControllerUtility.PATH_VARIABLE ) Long id, RedirectAttributes attributes ) {

        initEdit( attributes );
        attributes.addFlashAttribute( ControllerUtility.SHOW_CREATE_EDIT_PANEL, true );

        attributes.addFlashAttribute( getDtoVariableName( ), getService( ).getById( id ) );

        return Requests.redirectTo( getPagePath( ) );
    }





    @PostMapping( RequestPath.SAVE )
    public String save( @ModelAttribute @Validated( BeanValidation.class ) Dto dto,
                        BindingResult bindingResult,
                        Model model,
                        RedirectAttributes attributes ) {

        if ( bindingResult.hasErrors( ) ) {
            model.addAttribute( ControllerUtility.SHOW_CREATE_EDIT_PANEL, true );
            return page( Optional.empty( ), Optional.empty( ), model );
        }

        boolean idEqualsNull = Objects.isNull( getHelperDto( dto ).getId( ) );
        List< String > alreadyExistedProperties = getService( ).getAlreadyExistProperties( dto );

        if ( Lists.isNotEmpty( alreadyExistedProperties ) ) {
            attributes.addFlashAttribute( ControllerUtility.SHOW_CREATE_EDIT_PANEL, true );
            attributes.addFlashAttribute( getDtoVariableName( ), dto );
            Messages.addAlreadyPresent( alreadyExistedProperties, attributes );
        } else if ( idEqualsNull && save( dto ) ) {
            Messages.addSuccess( MessageType.SAVE_SUCCESS, getMessageDetail( dto ), attributes );
        } else if ( idEqualsNull ) {
            Messages.addError( MessageType.SAVE_FAILED, getMessageDetail( dto ), attributes );
        } else if ( update( dto ) ) {
            Messages.addSuccess( MessageType.UPDATE_SUCCESS, getMessageDetail( dto ), attributes );
        } else {
            Messages.addError( MessageType.UPDATE_FAILED, getMessageDetail( dto ), attributes );
        }

        return Requests.redirectTo( getPagePath( ) );
    }





    @GetMapping( RequestPath.DELETE )
    public String delete( @PathVariable( ControllerUtility.PATH_VARIABLE ) Long id, RedirectAttributes attributes ) {

        AbstractProfileHelperDto helperDto = ( AbstractProfileHelperDto ) getService( ).getById( id );
        helperDto.setDeletedByAdminDto( AuthenticationUtility.getCurrentLoggedAdminDto( ) );

        if ( getService( ).delete( helperDto ) ) {
            Messages.addSuccess( MessageType.DELETE_SUCCESS, getMessageDetail( ( Dto ) helperDto ), attributes );
        } else {
            Messages.addError( MessageType.DELETE_FAILED, getMessageDetail( ( Dto ) helperDto ), attributes );
        }

        return Requests.redirectTo( getPagePath( ) );
    }





    private String getMessageDetail( Dto dto ) {

        return Strings.concatWithColonBetween( getEntityName( ), getHelperDto( dto ).getName( ) );
    }

}
