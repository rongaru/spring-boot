package com.demo.springboot.abstrcats.dto;

import com.demo.springboot.dto.AdminDto;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public abstract class AbstractCreatedByHelperDto extends AbstractStatusHelperDto {

    private AdminDto createdByAdminDto;

    private Date createdDate;





    public AdminDto getCreatedByAdminDto( ) {

        if ( createdByAdminDto == null ) {
            createdByAdminDto = new AdminDto( );
        }

        return createdByAdminDto;
    }

}
