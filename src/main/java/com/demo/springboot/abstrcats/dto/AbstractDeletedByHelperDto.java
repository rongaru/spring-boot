package com.demo.springboot.abstrcats.dto;

import com.demo.springboot.dto.AdminDto;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public abstract class AbstractDeletedByHelperDto extends AbstractUpdatedByHelperDto {

    private AdminDto deletedByAdminDto;

    private Date deletedDate;





    public AdminDto getDeletedByAdminDto( ) {

        if ( deletedByAdminDto == null ) {
            deletedByAdminDto = new AdminDto( );
        }

        return deletedByAdminDto;
    }

}
