package com.demo.springboot.abstrcats.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public abstract class AbstractPersonHelperDto extends AbstractDeletedByHelperDto {

    private String firstName;

    private String middleName;

    private String lastName;

    private String gender;

    private Date dateOfBirth;

    private String email;

    private String phone;

    private String address;





    public String getFullName( ) {

        return ( middleName == null ) ? firstName + " " + lastName : firstName + " " + middleName + " " + lastName;
    }

}
