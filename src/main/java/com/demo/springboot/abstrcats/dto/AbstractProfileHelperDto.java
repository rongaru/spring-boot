package com.demo.springboot.abstrcats.dto;

import com.demo.springboot.Utility.validation.NotNullValidation;
import com.demo.springboot.Utility.validation.SizeValidation;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public abstract class AbstractProfileHelperDto extends AbstractDeletedByHelperDto {

    @NotBlank( message = "Name is required.", groups = NotNullValidation.class )
    @Size( max = 20, message = "Name must be less than 20 characters.", groups = SizeValidation.class )
    private String name;

    @Size( max = 50, message = "Description must be less than 50 characters.", groups = SizeValidation.class )
    private String description;

}
