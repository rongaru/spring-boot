package com.demo.springboot.abstrcats.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractStatusHelperDto extends AbstractEntityHelperDto {

    private String status;

}
