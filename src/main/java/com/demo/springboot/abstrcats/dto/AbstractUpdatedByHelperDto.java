package com.demo.springboot.abstrcats.dto;

import com.demo.springboot.dto.AdminDto;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public abstract class AbstractUpdatedByHelperDto extends AbstractCreatedByHelperDto {

    private AdminDto updatedByAdminDto;

    private Date updatedDate;





    public AdminDto getUpdatedByAdminDto( ) {

        if ( updatedByAdminDto == null ) {
            updatedByAdminDto = new AdminDto( );
        }

        return updatedByAdminDto;
    }

}
