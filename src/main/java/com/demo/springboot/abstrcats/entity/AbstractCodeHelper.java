package com.demo.springboot.abstrcats.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractCodeHelper extends AbstractProfileHelper {

    @Column( name = "CODE", nullable = false, length = 20 )
    private String code;

}
