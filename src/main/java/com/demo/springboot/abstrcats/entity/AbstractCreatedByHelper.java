package com.demo.springboot.abstrcats.entity;

import com.demo.springboot.entity.Admin;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractCreatedByHelper extends AbstractStatusHelper {

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "CREATED_BY_USER", nullable = false, referencedColumnName = "ID" )
    private Admin createdByAdmin;

    @Temporal( TemporalType.DATE )
    @Column( name = "CREATED_DATE", nullable = true )
    private Date createdDate;

}
