package com.demo.springboot.abstrcats.entity;

import com.demo.springboot.entity.Admin;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractDeletedByHelper extends AbstractUpdatedByHelper {

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "DELETED_BY_USER", nullable = true, referencedColumnName = "ID" )
    private Admin deletedByAdmin;

    @Temporal( TemporalType.DATE )
    @Column( name = "DELETED_DATE", nullable = true )
    private Date deletedDate;

}
