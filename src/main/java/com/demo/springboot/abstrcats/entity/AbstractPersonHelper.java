package com.demo.springboot.abstrcats.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractPersonHelper extends AbstractDeletedByHelper {

    @Column( name = "FIRST_NAME", nullable = false, length = 20 )
    private String firstName;

    @Column( name = "MIDDLE_NAME", nullable = true, length = 20 )
    private String middleName;

    @Column( name = "LAST_NAME", nullable = false, length = 20 )
    private String lastName;

    @Column( name = "GENDER", nullable = false, length = 6 )
    private String gender;

    @Temporal( TemporalType.DATE )
    @Column( name = "DATE_OF_BIRTH", nullable = false )
    private Date dateOfBirth;

    @Column( name = "EMAIL", nullable = true, length = 20 )
    private String email;

    @Column( name = "PHONE", nullable = false, length = 10 )
    private String phone;

    @Column( name = "ADDRESS", nullable = false, length = 20 )
    private String address;

}
