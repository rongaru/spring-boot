package com.demo.springboot.abstrcats.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractProfileHelper extends AbstractDeletedByHelper {

    @Column( name = "NAME", nullable = false, length = 20 )
    private String name;

    @Column( name = "DESCRIPTION", nullable = true, length = 50 )
    private String description;

}
