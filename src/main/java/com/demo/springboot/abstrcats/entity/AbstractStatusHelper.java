package com.demo.springboot.abstrcats.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractStatusHelper extends AbstractEntityHelper {

    @Column( name = "STATUS", nullable = false, length = 15 )
    private String status;

}
