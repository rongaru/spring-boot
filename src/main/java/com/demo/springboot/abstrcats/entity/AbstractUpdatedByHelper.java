package com.demo.springboot.abstrcats.entity;

import com.demo.springboot.entity.Admin;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractUpdatedByHelper extends AbstractCreatedByHelper {

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "UPDATED_BY_USER", nullable = true, referencedColumnName = "ID" )
    private Admin updatedByAdmin;

    @Temporal( TemporalType.DATE )
    @Column( name = "UPDATED_DATE", nullable = true )
    private Date updatedDate;

}
