package com.demo.springboot.abstrcats.mapper;

import com.demo.springboot.abstrcats.dto.AbstractCodeHelperDto;
import com.demo.springboot.abstrcats.entity.AbstractCodeHelper;

public abstract class AbstractCodeHelperMapper {

    protected static void mapSuperClassProperties( AbstractCodeHelper abstractCodeHelper, AbstractCodeHelperDto abstractCodeHelperDto ) {

        abstractCodeHelperDto.setCode( abstractCodeHelper.getCode( ) );
        AbstractProfileHelperMapper.mapSuperClassProperties( abstractCodeHelper, abstractCodeHelperDto );
    }

}
