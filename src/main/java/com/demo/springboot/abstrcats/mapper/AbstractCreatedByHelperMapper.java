package com.demo.springboot.abstrcats.mapper;

import com.demo.springboot.abstrcats.dto.AbstractCreatedByHelperDto;
import com.demo.springboot.abstrcats.entity.AbstractCreatedByHelper;

public abstract class AbstractCreatedByHelperMapper {

    protected static void mapSuperClassProperties( AbstractCreatedByHelper abstractCreatedByHelper, AbstractCreatedByHelperDto abstractCreatedByHelperDto ) {

        abstractCreatedByHelperDto.getCreatedByAdminDto( )
                                  .setUsername( abstractCreatedByHelper.getCreatedByAdmin( )
                                                                       .getUsername( ) );
        abstractCreatedByHelperDto.setCreatedDate( abstractCreatedByHelper.getCreatedDate( ) );
        AbstractStatusHelperMapper.mapSuperClassProperties( abstractCreatedByHelper, abstractCreatedByHelperDto );
    }

}
