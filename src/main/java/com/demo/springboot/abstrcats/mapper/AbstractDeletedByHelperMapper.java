package com.demo.springboot.abstrcats.mapper;


import com.demo.springboot.abstrcats.dto.AbstractDeletedByHelperDto;
import com.demo.springboot.abstrcats.entity.AbstractDeletedByHelper;

public abstract class AbstractDeletedByHelperMapper {

    protected static void mapSuperClassProperties( AbstractDeletedByHelper abstractDeletedByHelper, AbstractDeletedByHelperDto abstractDeletedByHelperDto ) {

        if ( abstractDeletedByHelper.getDeletedByAdmin( ) != null ) {
            abstractDeletedByHelperDto.getDeletedByAdminDto( )
                                      .setUsername( abstractDeletedByHelper.getDeletedByAdmin( )
                                                                           .getUsername( ) );
            abstractDeletedByHelperDto.setDeletedDate( abstractDeletedByHelper.getDeletedDate( ) );
        }
        AbstractUpdatedByHelperMapper.mapSuperClassProperties( abstractDeletedByHelper, abstractDeletedByHelperDto );
    }

}
