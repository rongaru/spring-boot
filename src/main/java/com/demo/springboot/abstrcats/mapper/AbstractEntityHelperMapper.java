package com.demo.springboot.abstrcats.mapper;


import com.demo.springboot.abstrcats.dto.AbstractEntityHelperDto;
import com.demo.springboot.abstrcats.entity.AbstractEntityHelper;

public abstract class AbstractEntityHelperMapper {

    protected static void mapSuperClassProperties( AbstractEntityHelper abstractEntityHelper, AbstractEntityHelperDto abstractEntityHelperDto ) {

        abstractEntityHelperDto.setId( abstractEntityHelper.getId( ) );
    }

}
