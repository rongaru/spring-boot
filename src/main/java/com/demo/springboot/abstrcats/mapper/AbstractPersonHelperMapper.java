package com.demo.springboot.abstrcats.mapper;

import com.demo.springboot.abstrcats.dto.AbstractPersonHelperDto;
import com.demo.springboot.abstrcats.entity.AbstractPersonHelper;

public abstract class AbstractPersonHelperMapper {

    protected static void mapSuperClassProperties( AbstractPersonHelper abstractPersonHelper, AbstractPersonHelperDto abstractPersonHelperDto ) {

        abstractPersonHelperDto.setAddress( abstractPersonHelper.getAddress( ) );
        abstractPersonHelperDto.setEmail( abstractPersonHelper.getEmail( ) );
        abstractPersonHelperDto.setFirstName( abstractPersonHelper.getFirstName( ) );
        abstractPersonHelperDto.setGender( abstractPersonHelper.getGender( ) );
        abstractPersonHelperDto.setLastName( abstractPersonHelper.getLastName( ) );
        abstractPersonHelperDto.setMiddleName( abstractPersonHelper.getMiddleName( ) );
        abstractPersonHelperDto.setPhone( abstractPersonHelper.getPhone( ) );
        AbstractDeletedByHelperMapper.mapSuperClassProperties( abstractPersonHelper, abstractPersonHelperDto );
    }

}
