package com.demo.springboot.abstrcats.mapper;

import com.demo.springboot.abstrcats.dto.AbstractProfileHelperDto;
import com.demo.springboot.abstrcats.entity.AbstractProfileHelper;

public abstract class AbstractProfileHelperMapper {

    protected static void mapSuperClassProperties( AbstractProfileHelper abstractProfileHelper, AbstractProfileHelperDto abstractProfileHelperDto ) {

        abstractProfileHelperDto.setDescription( abstractProfileHelper.getDescription( ) );
        abstractProfileHelperDto.setName( abstractProfileHelper.getName( ) );
        AbstractDeletedByHelperMapper.mapSuperClassProperties( abstractProfileHelper, abstractProfileHelperDto );
    }

}
