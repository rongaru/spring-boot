package com.demo.springboot.abstrcats.mapper;

import com.demo.springboot.abstrcats.dto.AbstractStatusHelperDto;
import com.demo.springboot.abstrcats.entity.AbstractStatusHelper;

public abstract class AbstractStatusHelperMapper {

    protected static void mapSuperClassProperties( AbstractStatusHelper abstractStatusHelper, AbstractStatusHelperDto abstractStatusHelperDto ) {

        abstractStatusHelperDto.setStatus( abstractStatusHelper.getStatus( ) );
        AbstractEntityHelperMapper.mapSuperClassProperties( abstractStatusHelper, abstractStatusHelperDto );
    }

}
