package com.demo.springboot.abstrcats.mapper;

import com.demo.springboot.abstrcats.dto.AbstractUpdatedByHelperDto;
import com.demo.springboot.abstrcats.entity.AbstractUpdatedByHelper;

public abstract class AbstractUpdatedByHelperMapper {

    protected static void mapSuperClassProperties( AbstractUpdatedByHelper abstractUpdatedByHelper, AbstractUpdatedByHelperDto abstractUpdatedByHelperDto ) {

        if ( abstractUpdatedByHelper.getUpdatedByAdmin( ) != null ) {
            abstractUpdatedByHelperDto.getUpdatedByAdminDto( )
                                      .setUsername( abstractUpdatedByHelper.getUpdatedByAdmin( )
                                                                           .getUsername( ) );
            abstractUpdatedByHelperDto.setUpdatedDate( abstractUpdatedByHelper.getUpdatedDate( ) );
        }
        AbstractCreatedByHelperMapper.mapSuperClassProperties( abstractUpdatedByHelper, abstractUpdatedByHelperDto );
    }

}
