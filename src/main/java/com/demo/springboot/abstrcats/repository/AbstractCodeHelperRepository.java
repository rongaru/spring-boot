package com.demo.springboot.abstrcats.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AbstractCodeHelperRepository< Entity > extends AbstractProfileHelperRepository< Entity > {

    Long countByCode( String code );


    List< Entity > findByCode( String code );


    Page< Entity > findByCode( String code, Pageable pageable );


    Long countByCodeAndStatusNotIn( String code, List< String > Statuses );


    List< Entity > findByCodeAndStatusNotIn( String code, List< String > Statuses );


    Page< Entity > findByCodeAndStatusNotIn( String code, Pageable pageable, List< String > Statuses );

}
