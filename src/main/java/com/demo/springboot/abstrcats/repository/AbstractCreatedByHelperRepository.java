package com.demo.springboot.abstrcats.repository;

import com.demo.springboot.entity.Admin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public interface AbstractCreatedByHelperRepository< Entity > extends AbstractStatusHelperRepository< Entity > {

    Long countByCreatedByAdmin( Admin createdByAdmin );


    List< Entity > findByCreatedByAdmin( Admin createdByAdmin );


    Page< Entity > findByCreatedByAdmin( Admin createdByAdmin, Pageable pageable );


    Long countByCreatedDate( Date createdDate );


    List< Entity > findByCreatedDate( Date createdDate );


    Page< Entity > findByCreatedDate( Date createdDate, Pageable pageable );


    Long countByCreatedByAdminAndStatusNotIn( Admin createdByAdmin, List< String > statuses );


    List< Entity > findByCreatedByAdminAndStatusNotIn( Admin createdByAdmin, List< String > statuses );


    Page< Entity > findByCreatedByAdminAndStatusNotIn( Admin createdByAdmin, List< String > statuses, Pageable pageable );


    Long countByCreatedDateAndStatusNotIn( Date createdDate, List< String > statuses );


    List< Entity > findByCreatedDateAndStatusNotIn( Date createdDate, List< String > statuses );


    Page< Entity > findByCreatedDateAndStatusNotIn( Date createdDate, List< String > statuses, Pageable pageable );

}
