package com.demo.springboot.abstrcats.repository;

import com.demo.springboot.entity.Admin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public interface AbstractDeletedByHelperRepository< Entity > extends AbstractUpdatedByHelperRepository< Entity > {

    Long countByDeletedByAdmin( Admin deletedByAdmin );


    List< Entity > findByDeletedByAdmin( Admin deletedByAdmin );


    Page< Entity > findByDeletedByAdmin( Admin deletedByAdmin, Pageable pageable );


    Long countByDeletedDate( Date deletedDate );


    List< Entity > findByDeletedDate( Date deletedDate );


    Page< Entity > findByDeletedDate( Date deletedDate, Pageable pageable );


    Long countByDeletedByAdminAndStatusNotIn( Admin deletedByAdmin, List< String > statuses );


    List< Entity > findByDeletedByAdminAndStatusNotIn( Admin deletedByAdmin, List< String > statuses );


    Page< Entity > findByDeletedByAdminAndStatusNotIn( Admin deletedByAdmin, List< String > statuses, Pageable pageable );


    Long countByDeletedDateAndStatusNotIn( Date deletedDate, List< String > statuses );


    List< Entity > findByDeletedDateAndStatusNotIn( Date deletedDate, List< String > statuses );


    Page< Entity > findByDeletedDateAndStatusNotIn( Date deletedDate, List< String > statuses, Pageable pageable );

}
