package com.demo.springboot.abstrcats.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public interface AbstractPersonHelperRepository< Entity > extends AbstractDeletedByHelperRepository< Entity > {

    Long countByFirstName( String firstName );


    List< Entity > findByFirstName( String firstName );


    Page< Entity > findByFirstName( String firstName, Pageable pageable );


    Long countByFirstNameAndStatusNotIn( String firstName, List< String > statuses );


    List< Entity > findByFirstNameAndStatusNotIn( String firstName, List< String > statuses );


    Page< Entity > findByFirstNameAndStatusNotIn( String firstName, Pageable pageable, List< String > statuses );


    Long countByMiddleName( String middleName );


    List< Entity > findByMiddleName( String middleName );


    Page< Entity > findByMiddleName( String middleName, Pageable pageable );


    Long countByMiddleNameAndStatusNotIn( String middleName, List< String > statuses );


    List< Entity > findByMiddleNameAndStatusNotIn( String middleName, List< String > statuses );


    Page< Entity > findByMiddleNameAndStatusNotIn( String middleName, Pageable pageable, List< String > statuses );


    Long countByLastName( String lastName );


    List< Entity > findByLastName( String lastName );


    Page< Entity > findByLastName( String lastName, Pageable pageable );


    Long countByLastNameAndStatusNotIn( String lastName, List< String > statuses );


    List< Entity > findByLastNameAndStatusNotIn( String lastName, List< String > statuses );


    Page< Entity > findByLastNameAndStatusNotIn( String lastName, Pageable pageable, List< String > statuses );


    Long countByGender( String gender );


    List< Entity > findByGender( String gender );


    Page< Entity > findByGender( String gender, Pageable pageable );


    Long countByGenderAndStatusNotIn( String gender, List< String > statuses );


    List< Entity > findByGenderAndStatusNotIn( String gender, List< String > statuses );


    Page< Entity > findByGenderAndStatusNotIn( String gender, Pageable pageable, List< String > statuses );


    Long countByDateOfBirth( Date dateOfBirth );


    List< Entity > findByDateOfBirth( Date dateOfBirth );


    Page< Entity > findByDateOfBirth( Date dateOfBirth, Pageable pageable );


    Long countByDateOfBirthAndStatusNotIn( Date dateOfBirth, List< String > statuses );


    List< Entity > findByDateOfBirthAndStatusNotIn( Date dateOfBirth, List< String > statuses );


    Page< Entity > findByDateOfBirthAndStatusNotIn( Date dateOfBirth, Pageable pageable, List< String > statuses );


    Long countByEmail( String email );


    List< Entity > findByEmail( String email );


    Page< Entity > findByEmail( String email, Pageable pageable );


    Long countByEmailAndStatusNotIn( String email, List< String > statuses );


    List< Entity > findByEmailAndStatusNotIn( String email, List< String > statuses );


    Page< Entity > findByEmailAndStatusNotIn( String email, Pageable pageable, List< String > statuses );


    Long countByPhone( String phone );


    List< Entity > findByPhone( String phone );


    Page< Entity > findByPhone( String phone, Pageable pageable );


    Long countByPhoneAndStatusNotIn( String phone, List< String > statuses );


    List< Entity > findByPhoneAndStatusNotIn( String phone, List< String > statuses );


    Page< Entity > findByPhoneAndStatusNotIn( String phone, Pageable pageable, List< String > statuses );


    Long countByAddress( String address );


    List< Entity > findByAddress( String address );


    Page< Entity > findByAddress( String address, Pageable pageable );


    Long countByAddressAndStatusNotIn( String address, List< String > statuses );


    List< Entity > findByAddressAndStatusNotIn( String address, List< String > statuses );


    Page< Entity > findByAddressAndStatusNotIn( String address, Pageable pageable, List< String > statuses );

}
