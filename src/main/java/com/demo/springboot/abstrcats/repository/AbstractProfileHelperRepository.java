package com.demo.springboot.abstrcats.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AbstractProfileHelperRepository< Entity > extends AbstractDeletedByHelperRepository< Entity > {

    Long countByName( String name );


    List< Entity > findByName( String name );


    Page< Entity > findByName( String name, Pageable pageable );


    Long countByNameAndStatusNotIn( String name, List< String > statuses );


    List< Entity > findByNameAndStatusNotIn( String name, List< String > statuses );


    Page< Entity > findByNameAndStatusNotIn( String name, Pageable pageable, List< String > statuses );


    Long countByDescription( String description );


    List< Entity > findByDescription( String description );


    Page< Entity > findByDescription( String description, Pageable pageable );


    Long countByDescriptionAndStatusNotIn( String description, List< String > statuses );


    List< Entity > findByDescriptionAndStatusNotIn( String description, List< String > statuses );


    Page< Entity > findByDescriptionAndStatusNotIn( String description, Pageable pageable, List< String > statuses );

}
