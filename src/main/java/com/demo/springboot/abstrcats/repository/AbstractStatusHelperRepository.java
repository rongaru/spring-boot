package com.demo.springboot.abstrcats.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AbstractStatusHelperRepository< Entity > {

    Long countByStatus( String status );


    List< Entity > findByStatus( String status );


    Page< Entity > findByStatus( String status, Pageable pageable );


    Long countByStatusNotIn( List< String > statuses );


    List< Entity > findByStatusNotIn( List< String > statuses );


    Page< Entity > findByStatusNotIn( List< String > statuses, Pageable pageable );

}
