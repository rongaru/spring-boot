package com.demo.springboot.abstrcats.repository;

import com.demo.springboot.entity.Admin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public interface AbstractUpdatedByHelperRepository< Entity > extends AbstractCreatedByHelperRepository< Entity > {

    Long countByUpdatedByAdmin( Admin updatedByAdmin );


    List< Entity > findByUpdatedByAdmin( Admin updatedByAdmin );


    Page< Entity > findByUpdatedByAdmin( Admin updatedByAdmin, Pageable pageable );


    Long countByUpdatedDate( Date updatedDate );


    List< Entity > findByUpdatedDate( Date updatedDate );


    Page< Entity > findByUpdatedDate( Date updatedDate, Pageable pageable );


    Long countByUpdatedByAdminAndStatusNotIn( Admin updatedByAdmin, List< String > statuses );


    List< Entity > findByUpdatedByAdminAndStatusNotIn( Admin updatedByAdmin, List< String > statuses );


    Page< Entity > findByUpdatedByAdminAndStatusNotIn( Admin updatedByAdmin, List< String > statuses, Pageable pageable );


    Long countByUpdatedDateAndStatusNotIn( Date updatedDate, List< String > statuses );


    List< Entity > findByUpdatedDateAndStatusNotIn( Date updatedDate, List< String > statuses );


    Page< Entity > findByUpdatedDateAndStatusNotIn( Date updatedDate, List< String > statuses, Pageable pageable );

}
