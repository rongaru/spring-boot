package com.demo.springboot.abstrcats.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AbstractCodeHelperService< Dto > extends AbstractProfileHelperService< Dto > {

    Long getCountByCode( String code );


    List< Dto > getByCode( String code );


    Page< Dto > getByCode( String code, Pageable pageable );


    Long getCountByCodeAndStatusNotIn( String code, List< String > Statuses );


    List< Dto > getByCodeAndStatusNotIn( String code, List< String > Statuses );


    Page< Dto > getByCodeAndStatusNotIn( String code, Pageable pageable, List< String > Statuses );

}
