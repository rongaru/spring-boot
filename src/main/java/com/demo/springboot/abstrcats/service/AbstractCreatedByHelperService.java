package com.demo.springboot.abstrcats.service;

import com.demo.springboot.dto.AdminDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public interface AbstractCreatedByHelperService< Dto > extends AbstractStatusHelperService< Dto > {

    boolean save( Dto dto );


    List< String > getAlreadyExistProperties( Dto dto );


    Long getCountByCreatedByAdmin( AdminDto createdByAdminDto );


    List< Dto > getByCreatedByAdmin( AdminDto createdByAdminDto );


    Page< Dto > getByCreatedByAdmin( AdminDto createdByAdminDto, Pageable pageable );


    Long getCountByCreatedDate( Date createdDate );


    List< Dto > getByCreatedDate( Date createdDate );


    Page< Dto > getByCreatedDate( Date createdDate, Pageable pageable );


    Long getCountByCreatedByAdminAndStatusNotIn( AdminDto createdByAdminDto, List< String > statuses );


    List< Dto > getByCreatedByAdminAndStatusNotIn( AdminDto createdByAdminDto, List< String > statuses );


    Page< Dto > getByCreatedByAdminAndStatusNotIn( AdminDto createdByAdminDto, List< String > statuses, Pageable pageable );


    Long getCountByCreatedDateAndStatusNotIn( Date createdDate, List< String > statuses );


    List< Dto > getByCreatedDateAndStatusNotIn( Date createdDate, List< String > statuses );


    Page< Dto > getByCreatedDateAndStatusNotIn( Date createdDate, List< String > statuses, Pageable pageable );

}
