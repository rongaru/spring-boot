package com.demo.springboot.abstrcats.service;

import com.demo.springboot.dto.AdminDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public interface AbstractDeletedByHelperService< Dto > extends AbstractUpdatedByHelperService< Dto > {

    boolean delete( Dto dto );


    Long getCountByDeletedByAdmin( AdminDto deletedByAdminDto );


    List< Dto > getByDeletedByAdmin( AdminDto deletedByAdminDto );


    Page< Dto > getByDeletedByAdmin( AdminDto deletedByAdminDto, Pageable pageable );


    Long getCountByDeletedDate( Date deletedDate );


    List< Dto > getByDeletedDate( Date deletedDate );


    Page< Dto > getByDeletedDate( Date deletedDate, Pageable pageable );


    Long getCountByDeletedByAdminAndStatusNotIn( AdminDto deletedByAdminDto, List< String > statuses );


    List< Dto > getByDeletedByAdminAndStatusNotIn( AdminDto deletedByAdminDto, List< String > statuses );


    Page< Dto > getByDeletedByAdminAndStatusNotIn( AdminDto deletedByAdminDto, List< String > statuses, Pageable pageable );


    Long getCountByDeletedDateAndStatusNotIn( Date deletedDate, List< String > statuses );


    List< Dto > getByDeletedDateAndStatusNotIn( Date deletedDate, List< String > statuses );


    Page< Dto > getByDeletedDateAndStatusNotIn( Date deletedDate, List< String > statuses, Pageable pageable );

}
