package com.demo.springboot.abstrcats.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AbstractEntityHelperService< Dto > {

    Long getCount( );


    List< Dto > getAll( );


    Page< Dto > getAll( Pageable pageable );


    Dto getById( Long id );

}
