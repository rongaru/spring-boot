package com.demo.springboot.abstrcats.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public interface AbstractPersonHelperService< Dto > extends AbstractDeletedByHelperService< Dto > {

    Long getCountByFirstName( String firstName );


    List< Dto > getByFirstName( String firstName );


    Page< Dto > getByFirstName( String firstName, Pageable pageable );


    Long getCountByFirstNameAndStatusNotIn( String firstName, List< String > statuses );


    List< Dto > getByFirstNameAndStatusNotIn( String firstName, List< String > statuses );


    Page< Dto > getByFirstNameAndStatusNotIn( String firstName, Pageable pageable, List< String > statuses );


    Long getCountByMiddleName( String middleName );


    List< Dto > getByMiddleName( String middleName );


    Page< Dto > getByMiddleName( String middleName, Pageable pageable );


    Long getCountByMiddleNameAndStatusNotIn( String middleName, List< String > statuses );


    List< Dto > getByMiddleNameAndStatusNotIn( String middleName, List< String > statuses );


    Page< Dto > getByMiddleNameAndStatusNotIn( String middleName, Pageable pageable, List< String > statuses );


    Long getCountByLastName( String lastName );


    List< Dto > getByLastName( String lastName );


    Page< Dto > getByLastName( String lastName, Pageable pageable );


    Long getCountByLastNameAndStatusNotIn( String lastName, List< String > statuses );


    List< Dto > getByLastNameAndStatusNotIn( String lastName, List< String > statuses );


    Page< Dto > getByLastNameAndStatusNotIn( String lastName, Pageable pageable, List< String > statuses );


    Long getCountByGender( String gender );


    List< Dto > getByGender( String gender );


    Page< Dto > getByGender( String gender, Pageable pageable );


    Long getCountByGenderAndStatusNotIn( String gender, List< String > statuses );


    List< Dto > getByGenderAndStatusNotIn( String gender, List< String > statuses );


    Page< Dto > getByGenderAndStatusNotIn( String gender, Pageable pageable, List< String > statuses );


    Long getCountByDateOfBirth( Date dateOfBirth );


    List< Dto > getByDateOfBirth( Date dateOfBirth );


    Page< Dto > getByDateOfBirth( Date dateOfBirth, Pageable pageable );


    Long getCountByDateOfBirthAndStatusNotIn( Date dateOfBirth, List< String > statuses );


    List< Dto > getByDateOfBirthAndStatusNotIn( Date dateOfBirth, List< String > statuses );


    Page< Dto > getByDateOfBirthAndStatusNotIn( Date dateOfBirth, Pageable pageable, List< String > statuses );


    Long getCountByEmail( String email );


    List< Dto > getByEmail( String email );


    Page< Dto > getByEmail( String email, Pageable pageable );


    Long getCountByEmailAndStatusNotIn( String email, List< String > statuses );


    List< Dto > getByEmailAndStatusNotIn( String email, List< String > statuses );


    Page< Dto > getByEmailAndStatusNotIn( String email, Pageable pageable, List< String > statuses );


    Long getCountByPhone( String phone );


    List< Dto > getByPhone( String phone );


    Page< Dto > getByPhone( String phone, Pageable pageable );


    Long getCountByPhoneAndStatusNotIn( String phone, List< String > statuses );


    List< Dto > getByPhoneAndStatusNotIn( String phone, List< String > statuses );


    Page< Dto > getByPhoneAndStatusNotIn( String phone, Pageable pageable, List< String > statuses );


    Long getCountByAddress( String address );


    List< Dto > getByAddress( String address );


    Page< Dto > getByAddress( String address, Pageable pageable );


    Long getCountByAddressAndStatusNotIn( String address, List< String > statuses );


    List< Dto > getByAddressAndStatusNotIn( String address, List< String > statuses );


    Page< Dto > getByAddressAndStatusNotIn( String address, Pageable pageable, List< String > statuses );

}
