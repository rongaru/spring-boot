package com.demo.springboot.abstrcats.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AbstractProfileHelperService< Dto > extends AbstractDeletedByHelperService< Dto > {

    Long getCountByName( String name );


    List< Dto > getByName( String name );


    Page< Dto > getByName( String name, Pageable pageable );


    Long getCountByNameAndStatusNotIn( String name, List< String > statuses );


    List< Dto > getByNameAndStatusNotIn( String name, List< String > statuses );


    Page< Dto > getByNameAndStatusNotIn( String name, Pageable pageable, List< String > statuses );


    Long getCountByDescription( String description );


    List< Dto > getByDescription( String description );


    Page< Dto > getByDescription( String description, Pageable pageable );


    Long getCountByDescriptionAndStatusNotIn( String description, List< String > statuses );


    List< Dto > getByDescriptionAndStatusNotIn( String description, List< String > statuses );


    Page< Dto > getByDescriptionAndStatusNotIn( String description, Pageable pageable, List< String > statuses );

}
