package com.demo.springboot.abstrcats.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AbstractStatusHelperService< Dto > extends AbstractEntityHelperService< Dto > {

    Long getCountByStatus( String status );


    List< Dto > getByStatus( String status );


    Page< Dto > getByStatus( String status, Pageable pageable );


    Long getCountByStatusNotIn( List< String > statuses );


    List< Dto > getByStatusNotIn( List< String > statuses );


    Page< Dto > getByStatusNotIn( List< String > statuses, Pageable pageable );

}
