package com.demo.springboot.abstrcats.service;

import com.demo.springboot.dto.AdminDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public interface AbstractUpdatedByHelperService< Dto > extends AbstractCreatedByHelperService< Dto > {

    boolean update( Dto dto );


    Long getCountByUpdatedByAdmin( AdminDto updatedByAdminDto );


    List< Dto > getByUpdatedByAdmin( AdminDto updatedByAdminDto );


    Page< Dto > getByUpdatedByAdmin( AdminDto updatedByAdminDto, Pageable pageable );


    Long getCountByUpdatedDate( Date updatedDate );


    List< Dto > getByUpdatedDate( Date updatedDate );


    Page< Dto > getByUpdatedDate( Date updatedDate, Pageable pageable );


    Long getCountByUpdatedByAdminAndStatusNotIn( AdminDto updatedByAdminDto, List< String > statuses );


    List< Dto > getByUpdatedByAdminAndStatusNotIn( AdminDto updatedByAdminDto, List< String > statuses );


    Page< Dto > getByUpdatedByAdminAndStatusNotIn( AdminDto updatedByAdminDto, List< String > statuses, Pageable pageable );


    Long getCountByUpdatedDateAndStatusNotIn( Date updatedDate, List< String > statuses );


    List< Dto > getByUpdatedDateAndStatusNotIn( Date updatedDate, List< String > statuses );


    Page< Dto > getByUpdatedDateAndStatusNotIn( Date updatedDate, List< String > statuses, Pageable pageable );

}
