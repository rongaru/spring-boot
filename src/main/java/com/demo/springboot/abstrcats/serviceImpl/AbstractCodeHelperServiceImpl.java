package com.demo.springboot.abstrcats.serviceImpl;

import com.demo.springboot.abstrcats.repository.AbstractCodeHelperRepository;
import com.demo.springboot.abstrcats.service.AbstractCodeHelperService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public abstract class AbstractCodeHelperServiceImpl< Entity, Dto > extends AbstractProfileHelperServiceImpl< Entity, Dto > implements AbstractCodeHelperService< Dto > {

    @Override
    protected abstract AbstractCodeHelperRepository getRepository( );





    @Override
    public Long getCountByCode( String code ) {

        return getRepository( ).countByCode( code );
    }





    @Override
    public List< Dto > getByCode( String code ) {

        return convertToDtos( getRepository( ).findByCode( code ) );
    }





    @Override
    public Page< Dto > getByCode( String code, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByCode( code, pageable ) );
    }





    @Override
    public Long getCountByCodeAndStatusNotIn( String code, List< String > Statuses ) {

        return getRepository( ).countByCodeAndStatusNotIn( code, Statuses );
    }





    @Override
    public List< Dto > getByCodeAndStatusNotIn( String code, List< String > Statuses ) {

        return convertToDtos( getRepository( ).findByCodeAndStatusNotIn( code, Statuses ) );
    }





    @Override
    public Page< Dto > getByCodeAndStatusNotIn( String code, Pageable pageable, List< String > Statuses ) {

        return convertToDtoPage( getRepository( ).findByCodeAndStatusNotIn( code, pageable, Statuses ) );
    }

}
