package com.demo.springboot.abstrcats.serviceImpl;

import com.demo.springboot.abstrcats.dto.AbstractCreatedByHelperDto;
import com.demo.springboot.abstrcats.entity.AbstractCreatedByHelper;
import com.demo.springboot.abstrcats.repository.AbstractCreatedByHelperRepository;
import com.demo.springboot.abstrcats.service.AbstractCreatedByHelperService;
import com.demo.springboot.constant.Status;
import com.demo.springboot.dto.AdminDto;
import com.demo.springboot.entity.Admin;
import com.demo.springboot.repository.AdminRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public abstract class AbstractCreatedByHelperServiceImpl< Entity, Dto > extends AbstractStatusHelperServiceImpl< Entity, Dto > implements AbstractCreatedByHelperService< Dto > {

    @Override
    protected abstract AbstractCreatedByHelperRepository getRepository( );


    protected abstract AdminRepository getAdminRepository( );


    protected abstract Entity getEntityInstance( );


    protected abstract void setCreateEditCommonParameters( Entity entity, Dto dto );





    protected Admin getAdmin( AdminDto adminDto ) {

        return getAdminRepository( ).getOne( adminDto.getId( ) );
    }





    @Override
    public Long getCountByCreatedByAdmin( AdminDto createdByAdminDto ) {

        return getRepository( ).countByCreatedByAdmin( getAdmin( createdByAdminDto ) );
    }





    @Override
    public List< Dto > getByCreatedByAdmin( AdminDto createdByAdminDto ) {

        return convertToDtos( getRepository( ).findByCreatedByAdmin( getAdmin( createdByAdminDto ) ) );
    }





    @Override
    public Page< Dto > getByCreatedByAdmin( AdminDto createdByAdminDto, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByCreatedByAdmin( getAdmin( createdByAdminDto ), pageable ) );
    }





    @Override
    public Long getCountByCreatedDate( Date createdDate ) {

        return getRepository( ).countByCreatedDate( createdDate );
    }





    @Override
    public List< Dto > getByCreatedDate( Date createdDate ) {

        return convertToDtos( getRepository( ).findByCreatedDate( createdDate ) );
    }





    @Override
    public Page< Dto > getByCreatedDate( Date createdDate, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByCreatedDate( createdDate, pageable ) );
    }





    @Override
    public Long getCountByCreatedByAdminAndStatusNotIn( AdminDto createdByAdminDto, List< String > statuses ) {

        return getRepository( ).countByCreatedByAdminAndStatusNotIn( getAdmin( createdByAdminDto ), statuses );
    }





    @Override
    public List< Dto > getByCreatedByAdminAndStatusNotIn( AdminDto createdByAdminDto, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByCreatedByAdminAndStatusNotIn( getAdmin( createdByAdminDto ),
                statuses ) );
    }





    @Override
    public Page< Dto > getByCreatedByAdminAndStatusNotIn( AdminDto createdByAdminDto,
                                                          List< String > statuses,
                                                          Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByCreatedByAdminAndStatusNotIn( getAdmin( createdByAdminDto ),
                statuses,
                pageable ) );
    }





    @Override
    public Long getCountByCreatedDateAndStatusNotIn( Date createdDate, List< String > statuses ) {

        return getRepository( ).countByCreatedDateAndStatusNotIn( createdDate, statuses );
    }





    @Override
    public List< Dto > getByCreatedDateAndStatusNotIn( Date createdDate, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByCreatedDateAndStatusNotIn( createdDate, statuses ) );
    }





    @Override
    public Page< Dto > getByCreatedDateAndStatusNotIn( Date createdDate, List< String > statuses, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByCreatedDateAndStatusNotIn( createdDate, statuses, pageable ) );
    }





    @Override
    public boolean save( Dto dto ) {

        AbstractCreatedByHelper abstractCreatedByHelper = ( AbstractCreatedByHelper ) getEntityInstance( );
        abstractCreatedByHelper.setCreatedByAdmin( getAdmin( ( ( AbstractCreatedByHelperDto ) dto ).getCreatedByAdminDto( ) ) );
        abstractCreatedByHelper.setCreatedDate( new Date( ) );
        abstractCreatedByHelper.setStatus( Status.CREATE_APPROVED );
        setCreateEditCommonParameters( ( Entity ) abstractCreatedByHelper, dto );
        getEntityRepository( ).save( abstractCreatedByHelper );

        return true;
    }

}
