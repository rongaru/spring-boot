package com.demo.springboot.abstrcats.serviceImpl;

import com.demo.springboot.abstrcats.dto.AbstractDeletedByHelperDto;
import com.demo.springboot.abstrcats.entity.AbstractDeletedByHelper;
import com.demo.springboot.abstrcats.repository.AbstractDeletedByHelperRepository;
import com.demo.springboot.abstrcats.service.AbstractDeletedByHelperService;
import com.demo.springboot.constant.Status;
import com.demo.springboot.dto.AdminDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public abstract class AbstractDeletedByHelperServiceImpl< Entity, Dto > extends AbstractUpdatedByHelperServiceImpl< Entity, Dto > implements AbstractDeletedByHelperService< Dto > {

    @Override
    protected abstract AbstractDeletedByHelperRepository getRepository( );





    @Override
    public Long getCountByDeletedByAdmin( AdminDto deletedByAdminDto ) {

        return getRepository( ).countByDeletedByAdmin( getAdmin( deletedByAdminDto ) );
    }





    @Override
    public List< Dto > getByDeletedByAdmin( AdminDto deletedByAdminDto ) {

        return convertToDtos( getRepository( ).findByDeletedByAdmin( getAdmin( deletedByAdminDto ) ) );
    }





    @Override
    public Page< Dto > getByDeletedByAdmin( AdminDto deletedByAdminDto, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByDeletedByAdmin( getAdmin( deletedByAdminDto ), pageable ) );
    }





    @Override
    public Long getCountByDeletedDate( Date deletedDate ) {

        return getRepository( ).countByDeletedDate( deletedDate );
    }





    @Override
    public List< Dto > getByDeletedDate( Date deletedDate ) {

        return convertToDtos( getRepository( ).findByDeletedDate( deletedDate ) );
    }





    @Override
    public Page< Dto > getByDeletedDate( Date deletedDate, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByDeletedDate( deletedDate, pageable ) );
    }





    @Override
    public Long getCountByDeletedByAdminAndStatusNotIn( AdminDto deletedByAdminDto, List< String > statuses ) {

        return getRepository( ).countByDeletedByAdminAndStatusNotIn( getAdmin( deletedByAdminDto ), statuses );
    }





    @Override
    public List< Dto > getByDeletedByAdminAndStatusNotIn( AdminDto deletedByAdminDto, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByDeletedByAdminAndStatusNotIn( getAdmin( deletedByAdminDto ),
                statuses ) );
    }





    @Override
    public Page< Dto > getByDeletedByAdminAndStatusNotIn( AdminDto deletedByAdminDto,
                                                          List< String > statuses,
                                                          Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByDeletedByAdminAndStatusNotIn( getAdmin( deletedByAdminDto ),
                statuses,
                pageable ) );
    }





    @Override
    public Long getCountByDeletedDateAndStatusNotIn( Date deletedDate, List< String > statuses ) {

        return getRepository( ).countByDeletedDateAndStatusNotIn( deletedDate, statuses );
    }





    @Override
    public List< Dto > getByDeletedDateAndStatusNotIn( Date deletedDate, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByDeletedDateAndStatusNotIn( deletedDate, statuses ) );

    }





    @Override
    public Page< Dto > getByDeletedDateAndStatusNotIn( Date deletedDate, List< String > statuses, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByDeletedDateAndStatusNotIn( deletedDate, statuses, pageable ) );
    }





    @Override
    public boolean delete( Dto dto ) {

        AbstractDeletedByHelperDto abstractDeletedByHelperDto = ( AbstractDeletedByHelperDto ) dto;

        AbstractDeletedByHelper abstractDeletedByHelper = ( AbstractDeletedByHelper ) getEntityRepository( ).findById(
                abstractDeletedByHelperDto.getId( ) )
                                                                                                            .get( );

        abstractDeletedByHelper.setDeletedByAdmin( getAdmin( abstractDeletedByHelperDto.getDeletedByAdminDto( ) ) );
        abstractDeletedByHelper.setDeletedDate( new Date( ) );
        abstractDeletedByHelper.setStatus( Status.DELETE_APPROVED );
        getEntityRepository( ).save( abstractDeletedByHelper );

        return true;
    }

}
