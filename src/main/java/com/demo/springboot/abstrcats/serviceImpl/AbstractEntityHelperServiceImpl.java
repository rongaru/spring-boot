package com.demo.springboot.abstrcats.serviceImpl;

import com.demo.springboot.abstrcats.service.AbstractEntityHelperService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract class AbstractEntityHelperServiceImpl< Entity, Dto > implements AbstractEntityHelperService< Dto > {

    protected abstract JpaRepository getEntityRepository( );


    protected abstract Dto convertToDto( Entity entity );


    protected abstract List< Dto > convertToDtos( List< Entity > entities );


    protected abstract Page< Dto > convertToDtoPage( Page< Entity > entities );





    @Override
    public Long getCount( ) {

        return getEntityRepository( ).count( );
    }





    @Override
    public List< Dto > getAll( ) {

        return convertToDtos( getEntityRepository( ).findAll( ) );
    }





    @Override
    public Page< Dto > getAll( Pageable pageable ) {

        return convertToDtoPage( getEntityRepository( ).findAll( pageable ) );
    }





    @Override
    public Dto getById( Long id ) {

        return convertToDto( ( Entity ) getEntityRepository( ).findById( id )
                                                              .get( ) );
    }

}
