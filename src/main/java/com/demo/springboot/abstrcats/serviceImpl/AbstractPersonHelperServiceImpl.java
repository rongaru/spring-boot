package com.demo.springboot.abstrcats.serviceImpl;

import com.demo.springboot.abstrcats.repository.AbstractPersonHelperRepository;
import com.demo.springboot.abstrcats.service.AbstractPersonHelperService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public abstract class AbstractPersonHelperServiceImpl< Entity, Dto > extends AbstractDeletedByHelperServiceImpl< Entity, Dto > implements AbstractPersonHelperService< Dto > {

    @Override
    protected abstract AbstractPersonHelperRepository getRepository( );





    @Override
    public Long getCountByFirstName( String firstName ) {

        return getRepository( ).countByFirstName( firstName );
    }





    @Override
    public List< Dto > getByFirstName( String firstName ) {

        return convertToDtos( getRepository( ).findByFirstName( firstName ) );
    }





    @Override
    public Page< Dto > getByFirstName( String firstName, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByFirstName( firstName, pageable ) );
    }





    @Override
    public Long getCountByFirstNameAndStatusNotIn( String firstName, List< String > statuses ) {

        return getRepository( ).countByFirstNameAndStatusNotIn( firstName, statuses );
    }





    @Override
    public List< Dto > getByFirstNameAndStatusNotIn( String firstName, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByFirstNameAndStatusNotIn( firstName, statuses ) );
    }





    @Override
    public Page< Dto > getByFirstNameAndStatusNotIn( String firstName, Pageable pageable, List< String > statuses ) {

        return convertToDtoPage( getRepository( ).findByFirstNameAndStatusNotIn( firstName, pageable, statuses ) );
    }





    @Override
    public Long getCountByMiddleName( String middleName ) {

        return getRepository( ).countByMiddleName( middleName );
    }





    @Override
    public List< Dto > getByMiddleName( String middleName ) {

        return convertToDtos( getRepository( ).findByMiddleName( middleName ) );
    }





    @Override
    public Page< Dto > getByMiddleName( String middleName, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByMiddleName( middleName, pageable ) );
    }





    @Override
    public Long getCountByMiddleNameAndStatusNotIn( String middleName, List< String > statuses ) {

        return getRepository( ).countByMiddleNameAndStatusNotIn( middleName, statuses );
    }





    @Override
    public List< Dto > getByMiddleNameAndStatusNotIn( String middleName, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByMiddleNameAndStatusNotIn( middleName, statuses ) );
    }





    @Override
    public Page< Dto > getByMiddleNameAndStatusNotIn( String middleName, Pageable pageable, List< String > statuses ) {

        return convertToDtoPage( getRepository( ).findByMiddleNameAndStatusNotIn( middleName, pageable, statuses ) );
    }





    @Override
    public Long getCountByLastName( String lastName ) {

        return getRepository( ).countByLastName( lastName );
    }





    @Override
    public List< Dto > getByLastName( String lastName ) {

        return convertToDtos( getRepository( ).findByLastName( lastName ) );
    }





    @Override
    public Page< Dto > getByLastName( String lastName, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByLastName( lastName, pageable ) );
    }





    @Override
    public Long getCountByLastNameAndStatusNotIn( String lastName, List< String > statuses ) {

        return getRepository( ).countByLastNameAndStatusNotIn( lastName, statuses );
    }





    @Override
    public List< Dto > getByLastNameAndStatusNotIn( String lastName, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByLastNameAndStatusNotIn( lastName, statuses ) );
    }





    @Override
    public Page< Dto > getByLastNameAndStatusNotIn( String lastName, Pageable pageable, List< String > statuses ) {

        return convertToDtoPage( getRepository( ).findByLastNameAndStatusNotIn( lastName, pageable, statuses ) );
    }





    @Override
    public Long getCountByGender( String gender ) {

        return getRepository( ).countByGender( gender );
    }





    @Override
    public List< Dto > getByGender( String gender ) {

        return convertToDtos( getRepository( ).findByGender( gender ) );
    }





    @Override
    public Page< Dto > getByGender( String gender, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByGender( gender, pageable ) );
    }





    @Override
    public Long getCountByGenderAndStatusNotIn( String gender, List< String > statuses ) {

        return getRepository( ).countByGenderAndStatusNotIn( gender, statuses );
    }





    @Override
    public List< Dto > getByGenderAndStatusNotIn( String gender, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByGenderAndStatusNotIn( gender, statuses ) );
    }





    @Override
    public Page< Dto > getByGenderAndStatusNotIn( String gender, Pageable pageable, List< String > statuses ) {

        return convertToDtoPage( getRepository( ).findByGenderAndStatusNotIn( gender, pageable, statuses ) );
    }





    @Override
    public Long getCountByDateOfBirth( Date dateOfBirth ) {

        return getRepository( ).countByDateOfBirth( dateOfBirth );
    }





    @Override
    public List< Dto > getByDateOfBirth( Date dateOfBirth ) {

        return convertToDtos( getRepository( ).findByDateOfBirth( dateOfBirth ) );
    }





    @Override
    public Page< Dto > getByDateOfBirth( Date dateOfBirth, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByDateOfBirth( dateOfBirth, pageable ) );
    }





    @Override
    public Long getCountByDateOfBirthAndStatusNotIn( Date dateOfBirth, List< String > statuses ) {

        return getRepository( ).countByDateOfBirthAndStatusNotIn( dateOfBirth, statuses );
    }





    @Override
    public List< Dto > getByDateOfBirthAndStatusNotIn( Date dateOfBirth, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByDateOfBirthAndStatusNotIn( dateOfBirth, statuses ) );
    }





    @Override
    public Page< Dto > getByDateOfBirthAndStatusNotIn( Date dateOfBirth, Pageable pageable, List< String > statuses ) {

        return convertToDtoPage( getRepository( ).findByDateOfBirthAndStatusNotIn( dateOfBirth, pageable, statuses ) );
    }





    @Override
    public Long getCountByEmail( String email ) {

        return getRepository( ).countByEmail( email );
    }





    @Override
    public List< Dto > getByEmail( String email ) {

        return convertToDtos( getRepository( ).findByEmail( email ) );
    }





    @Override
    public Page< Dto > getByEmail( String email, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByEmail( email, pageable ) );
    }





    @Override
    public Long getCountByEmailAndStatusNotIn( String email, List< String > statuses ) {

        return getRepository( ).countByEmailAndStatusNotIn( email, statuses );
    }





    @Override
    public List< Dto > getByEmailAndStatusNotIn( String email, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByEmailAndStatusNotIn( email, statuses ) );
    }





    @Override
    public Page< Dto > getByEmailAndStatusNotIn( String email, Pageable pageable, List< String > statuses ) {

        return convertToDtoPage( getRepository( ).findByEmailAndStatusNotIn( email, pageable, statuses ) );
    }





    @Override
    public Long getCountByPhone( String phone ) {

        return getRepository( ).countByPhone( phone );
    }





    @Override
    public List< Dto > getByPhone( String phone ) {

        return convertToDtos( getRepository( ).findByPhone( phone ) );
    }





    @Override
    public Page< Dto > getByPhone( String phone, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByPhone( phone, pageable ) );
    }





    @Override
    public Long getCountByPhoneAndStatusNotIn( String phone, List< String > statuses ) {

        return getRepository( ).countByPhoneAndStatusNotIn( phone, statuses );
    }





    @Override
    public List< Dto > getByPhoneAndStatusNotIn( String phone, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByPhoneAndStatusNotIn( phone, statuses ) );
    }





    @Override
    public Page< Dto > getByPhoneAndStatusNotIn( String phone, Pageable pageable, List< String > statuses ) {

        return convertToDtoPage( getRepository( ).findByPhoneAndStatusNotIn( phone, pageable, statuses ) );
    }





    @Override
    public Long getCountByAddress( String address ) {

        return getRepository( ).countByAddress( address );
    }





    @Override
    public List< Dto > getByAddress( String address ) {

        return convertToDtos( getRepository( ).findByAddress( address ) );
    }





    @Override
    public Page< Dto > getByAddress( String address, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByAddress( address, pageable ) );
    }





    @Override
    public Long getCountByAddressAndStatusNotIn( String address, List< String > statuses ) {

        return getRepository( ).countByAddressAndStatusNotIn( address, statuses );
    }





    @Override
    public List< Dto > getByAddressAndStatusNotIn( String address, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByAddressAndStatusNotIn( address, statuses ) );
    }





    @Override
    public Page< Dto > getByAddressAndStatusNotIn( String address, Pageable pageable, List< String > statuses ) {

        return convertToDtoPage( getRepository( ).findByAddressAndStatusNotIn( address, pageable, statuses ) );
    }

}
