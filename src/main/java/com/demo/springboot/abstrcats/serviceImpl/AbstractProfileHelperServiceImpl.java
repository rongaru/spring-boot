package com.demo.springboot.abstrcats.serviceImpl;

import com.demo.springboot.abstrcats.repository.AbstractProfileHelperRepository;
import com.demo.springboot.abstrcats.service.AbstractProfileHelperService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public abstract class AbstractProfileHelperServiceImpl< Entity, Dto > extends AbstractDeletedByHelperServiceImpl< Entity, Dto > implements AbstractProfileHelperService< Dto > {

    protected abstract AbstractProfileHelperRepository getRepository( );





    @Override
    public Long getCountByName( String name ) {

        return getRepository( ).countByName( name );
    }





    @Override
    public List< Dto > getByName( String name ) {

        return convertToDtos( getRepository( ).findByName( name ) );
    }





    @Override
    public Page< Dto > getByName( String name, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByName( name, pageable ) );
    }





    @Override
    public Long getCountByNameAndStatusNotIn( String name, List< String > statuses ) {

        return getRepository( ).countByNameAndStatusNotIn( name, statuses );
    }





    @Override
    public List< Dto > getByNameAndStatusNotIn( String name, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByNameAndStatusNotIn( name, statuses ) );
    }





    @Override
    public Page< Dto > getByNameAndStatusNotIn( String name, Pageable pageable, List< String > statuses ) {

        return convertToDtoPage( getRepository( ).findByNameAndStatusNotIn( name, pageable, statuses ) );
    }





    @Override
    public Long getCountByDescription( String description ) {

        return getRepository( ).countByDescription( description );
    }





    @Override
    public List< Dto > getByDescription( String description ) {

        return convertToDtos( getRepository( ).findByDescription( description ) );
    }





    @Override
    public Page< Dto > getByDescription( String description, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByDescription( description, pageable ) );
    }





    @Override
    public Long getCountByDescriptionAndStatusNotIn( String description, List< String > statuses ) {

        return getRepository( ).countByDescriptionAndStatusNotIn( description, statuses );
    }





    @Override
    public List< Dto > getByDescriptionAndStatusNotIn( String description, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByDescriptionAndStatusNotIn( description, statuses ) );
    }





    @Override
    public Page< Dto > getByDescriptionAndStatusNotIn( String description, Pageable pageable, List< String > statuses ) {

        return convertToDtoPage( getRepository( ).findByDescriptionAndStatusNotIn( description, pageable, statuses ) );
    }

}
