package com.demo.springboot.abstrcats.serviceImpl;

import com.demo.springboot.abstrcats.repository.AbstractStatusHelperRepository;
import com.demo.springboot.abstrcats.service.AbstractStatusHelperService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract class AbstractStatusHelperServiceImpl< Entity, Dto > extends AbstractEntityHelperServiceImpl< Entity, Dto > implements AbstractStatusHelperService< Dto > {

    protected abstract AbstractStatusHelperRepository getRepository( );





    @Override
    protected JpaRepository getEntityRepository( ) {

        return ( JpaRepository ) getRepository( );
    }





    @Override
    public Long getCountByStatus( String status ) {

        return getRepository( ).countByStatus( status );
    }





    @Override
    public List< Dto > getByStatus( String status ) {

        return convertToDtos( getRepository( ).findByStatus( status ) );
    }





    @Override
    public Page< Dto > getByStatus( String status, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByStatus( status, pageable ) );
    }





    @Override
    public Long getCountByStatusNotIn( List< String > statuses ) {

        return getRepository( ).countByStatusNotIn( statuses );
    }





    @Override
    public List< Dto > getByStatusNotIn( List< String > statuses ) {

        return convertToDtos( getRepository( ).findByStatusNotIn( statuses ) );
    }





    @Override
    public Page< Dto > getByStatusNotIn( List< String > statuses, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByStatusNotIn( statuses, pageable ) );
    }

}
