package com.demo.springboot.abstrcats.serviceImpl;

import com.demo.springboot.abstrcats.dto.AbstractUpdatedByHelperDto;
import com.demo.springboot.abstrcats.entity.AbstractEntityHelper;
import com.demo.springboot.abstrcats.entity.AbstractUpdatedByHelper;
import com.demo.springboot.abstrcats.repository.AbstractUpdatedByHelperRepository;
import com.demo.springboot.abstrcats.service.AbstractUpdatedByHelperService;
import com.demo.springboot.constant.Status;
import com.demo.springboot.dto.AdminDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public abstract class AbstractUpdatedByHelperServiceImpl< Entity, Dto > extends AbstractCreatedByHelperServiceImpl< Entity, Dto > implements AbstractUpdatedByHelperService< Dto > {

    @Override
    protected abstract AbstractUpdatedByHelperRepository getRepository( );





    private AbstractEntityHelper getHelperById( Long id ) {

        return ( AbstractEntityHelper ) getEntityRepository( ).findById( id )
                                                              .get( );
    }





    @Override
    public Long getCountByUpdatedByAdmin( AdminDto updatedByAdminDto ) {

        return getRepository( ).countByUpdatedByAdmin( getAdmin( updatedByAdminDto ) );
    }





    @Override
    public List< Dto > getByUpdatedByAdmin( AdminDto updatedByAdminDto ) {

        return convertToDtos( getRepository( ).findByUpdatedByAdmin( getAdmin( updatedByAdminDto ) ) );
    }





    @Override
    public Page< Dto > getByUpdatedByAdmin( AdminDto updatedByAdminDto, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByUpdatedByAdmin( getAdmin( updatedByAdminDto ), pageable ) );
    }





    @Override
    public Long getCountByUpdatedDate( Date updatedDate ) {

        return getRepository( ).countByUpdatedDate( updatedDate );
    }





    @Override
    public List< Dto > getByUpdatedDate( Date updatedDate ) {

        return convertToDtos( getRepository( ).findByUpdatedDate( updatedDate ) );
    }





    @Override
    public Page< Dto > getByUpdatedDate( Date updatedDate, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByUpdatedDate( updatedDate, pageable ) );
    }





    @Override
    public Long getCountByUpdatedByAdminAndStatusNotIn( AdminDto updatedByAdminDto, List< String > statuses ) {

        return getRepository( ).countByUpdatedByAdminAndStatusNotIn( getAdmin( updatedByAdminDto ), statuses );
    }





    @Override
    public List< Dto > getByUpdatedByAdminAndStatusNotIn( AdminDto updatedByAdminDto, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByUpdatedByAdminAndStatusNotIn( getAdmin( updatedByAdminDto ),
                statuses ) );
    }





    @Override
    public Page< Dto > getByUpdatedByAdminAndStatusNotIn( AdminDto updatedByAdminDto,
                                                          List< String > statuses,
                                                          Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByUpdatedByAdminAndStatusNotIn( getAdmin( updatedByAdminDto ),
                statuses,
                pageable ) );
    }





    @Override
    public Long getCountByUpdatedDateAndStatusNotIn( Date updatedDate, List< String > statuses ) {

        return getRepository( ).countByUpdatedDateAndStatusNotIn( updatedDate, statuses );
    }





    @Override
    public List< Dto > getByUpdatedDateAndStatusNotIn( Date updatedDate, List< String > statuses ) {

        return convertToDtos( getRepository( ).findByUpdatedDateAndStatusNotIn( updatedDate, statuses ) );
    }





    @Override
    public Page< Dto > getByUpdatedDateAndStatusNotIn( Date updatedDate, List< String > statuses, Pageable pageable ) {

        return convertToDtoPage( getRepository( ).findByUpdatedDateAndStatusNotIn( updatedDate, statuses, pageable ) );
    }





    @Override
    public boolean update( Dto dto ) {

        AbstractUpdatedByHelperDto helperDto = ( AbstractUpdatedByHelperDto ) dto;
        AbstractUpdatedByHelper abstractUpdatedByHelper = ( AbstractUpdatedByHelper ) getHelperById( helperDto.getId( ) );
        abstractUpdatedByHelper.setUpdatedByAdmin( getAdmin( helperDto.getUpdatedByAdminDto( ) ) );
        abstractUpdatedByHelper.setUpdatedDate( new Date( ) );
        abstractUpdatedByHelper.setStatus( Status.EDIT_APPROVED );
        setCreateEditCommonParameters( ( Entity ) abstractUpdatedByHelper, dto );
        getEntityRepository( ).save( abstractUpdatedByHelper );

        return true;
    }

}
