package com.demo.springboot.config;

import com.demo.springboot.Utility.Passwords;
import com.demo.springboot.serviceImpl.AdminServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;





    @Bean
    public AuthenticationProvider getAuthenticationProvider( ) {

        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider( );
        daoAuthenticationProvider.setUserDetailsService( userDetailsService );
        daoAuthenticationProvider.setPasswordEncoder( passwordEncoder( ) );
        return daoAuthenticationProvider;
    }





    @Bean
    public PasswordEncoder passwordEncoder( ) {

        return Passwords.getPasswordEncoder( );
    }





    @Bean
    @Override
    protected UserDetailsService userDetailsService( ) {

        return new AdminServiceImpl( );
    }





    @Override
    protected void configure( HttpSecurity http ) throws Exception {

        http
                .csrf( )
                .disable( )
                .authorizeRequests( )
                .antMatchers( "/", "/login*", "/error*", "/css/**", "/js/**", "/images/**", "/adminTemplate/**" )
                .permitAll( )
                .anyRequest( )
                .authenticated( )
                .and( )
                .formLogin( )
                .loginPage( "/login" )
                .defaultSuccessUrl( "/login-success", true )
                .failureUrl( "/login-error" )
                .and( )
                .logout( )
                .invalidateHttpSession( true )
                .clearAuthentication( true )
                .logoutRequestMatcher( new AntPathRequestMatcher( "/logout" ) )
                .logoutSuccessUrl( "/logout-success" );
    }

}
