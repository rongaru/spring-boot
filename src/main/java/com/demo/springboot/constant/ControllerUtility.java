package com.demo.springboot.constant;

public class ControllerUtility {

    public static final String DTO = "Dto";

    public static final String PAGE_NUMBER = "pageNumber";

    public static final String PAGE_SIZE = "pageSize";

    public static final String PATH_VARIABLE = "id";

    public static final String PAGE_NUMBERS = "pageNumbers";

    public static final String SHOW_CREATE_EDIT_PANEL = "showCreateEditPanel";

}
