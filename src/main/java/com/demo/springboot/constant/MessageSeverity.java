package com.demo.springboot.constant;

public class MessageSeverity {

    public static final String SUCCESS = "success";

    public static final String ERROR = "error";

    public static final String WARNING = "warning";

}
