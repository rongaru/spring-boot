package com.demo.springboot.constant;

public class MessageType {

    public static final String LOGIN_SUCCESS = "Welcome";

    public static final String LOGIN_SUCCESS_DETAIL = "Thank you for using our service.";

    public static final String LOGIN_ERROR = "Invalid Username/Password";

    public static final String LOGIN_ERROR_DETAIL = "Please enter valid username and password.";

    public static final String LOGOUT_SUCCESS = "Logout Success";

    public static final String LOGOUT_SUCCESS_DETAIL = "Please visit again.";

    public static final String LOGOUT_ERROR = "Logout Failed";

    public static final String LOGOUT_ERROR_DETAIL = "Something went wrong please try again.";

    public static final String SAVE_SUCCESS = "Save Success";

    public static final String UPDATE_SUCCESS = "Update Success";

    public static final String DELETE_SUCCESS = "Delete Success";

    public static final String SAVE_FAILED = "Save Failed";

    public static final String UPDATE_FAILED = "Update Failed";

    public static final String DELETE_FAILED = "Delete Failed";

    public static final String PROPERTIES_ALREADY_EXIST = "Properties Already Exist";

}
