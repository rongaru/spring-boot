package com.demo.springboot.constant;

public class RequestPath {

    public static final String ROOT = "/";

    public static final String SEPARATOR = "/";

    public static final String HOME = "home";

    public static final String LOGIN = "login";

    public static final String LOGIN_SUCCESS = "login_success";

    public static final String LOGIN_ERROR = "login_error";

    public static final String LOGOUT_SUCCESS = "logout_success";

    public static final String PAGE = "page";

    public static final String CREATE = "create";

    public static final String SAVE = "save";

    public static final String EDIT = "{id}/edit";

    public static final String DELETE = "{id}/delete";

}
