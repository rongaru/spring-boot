package com.demo.springboot.constant;

import java.util.Arrays;
import java.util.List;

public class Status {

    public static final String CREATE_PENDING = "CREATE_PENDING";

    public static final String CREATE_APPROVED = "CREATE_APPROVED";

    public static final String EDIT_PENDING = "EDIT_PENDING";

    public static final String EDIT_APPROVED = "EDIT_APPROVED";

    public static final String DELETE_PENDING = "DELETE_PENDING";

    public static final String DELETE_APPROVED = "DELETE_APPROVED";





    public static List< String > getDeleteStatuses( ) {

        return Arrays.asList( DELETE_PENDING, DELETE_APPROVED );
    }

}
