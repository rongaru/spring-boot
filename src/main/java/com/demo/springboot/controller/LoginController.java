package com.demo.springboot.controller;

import com.demo.springboot.Utility.Messages;
import com.demo.springboot.Utility.Requests;
import com.demo.springboot.constant.HtmlPages;
import com.demo.springboot.constant.MessageType;
import com.demo.springboot.constant.RequestPath;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController {

    @GetMapping( RequestPath.ROOT )
    public String root( Model model ) {

        return Requests.redirectTo( RequestPath.LOGIN );
    }





    @GetMapping( RequestPath.LOGIN )
    public String login( ) {

        return HtmlPages.LOGIN;
    }





    @GetMapping( RequestPath.LOGIN_SUCCESS )
    public String loginSuccess( RedirectAttributes attributes ) {

        Messages.addSuccess( MessageType.LOGIN_SUCCESS, MessageType.LOGIN_SUCCESS_DETAIL, attributes );
        return Requests.redirectTo( RequestPath.HOME );
    }





    @GetMapping( RequestPath.LOGIN_ERROR )
    public String loginFail( RedirectAttributes attributes ) {

        Messages.addError( MessageType.LOGIN_ERROR, MessageType.LOGIN_ERROR_DETAIL, attributes );
        return Requests.redirectTo( RequestPath.LOGIN );
    }





    @GetMapping( RequestPath.LOGOUT_SUCCESS )
    public String logoutSuccess( RedirectAttributes attributes ) {

        Messages.addSuccess( MessageType.LOGOUT_SUCCESS, MessageType.LOGOUT_SUCCESS_DETAIL, attributes );
        return Requests.redirectTo( RequestPath.LOGIN );
    }





    @GetMapping( RequestPath.HOME )
    public String home( ) {

        return HtmlPages.HOME;
    }

}
