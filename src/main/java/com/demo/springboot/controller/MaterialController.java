package com.demo.springboot.controller;

import com.demo.springboot.abstrcats.controller.AbstractController;
import com.demo.springboot.abstrcats.service.AbstractProfileHelperService;
import com.demo.springboot.dto.MaterialDto;
import com.demo.springboot.service.MaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping( "material" )
public class MaterialController extends AbstractController< MaterialDto > {

    @Autowired
    private MaterialService materialService;





    @Override
    protected AbstractProfileHelperService getService( ) {

        return materialService;
    }





    @Override
    protected MaterialDto getDtoInstance( ) {

        return new MaterialDto( );
    }





    @Override
    protected void initCreate( RedirectAttributes attributes ) {

    }





    @Override
    protected void initEdit( RedirectAttributes attributes ) {

    }

}
