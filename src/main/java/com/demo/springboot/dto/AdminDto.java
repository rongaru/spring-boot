package com.demo.springboot.dto;

import com.demo.springboot.abstrcats.dto.AbstractPersonHelperDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

@Getter
@Setter
public class AdminDto extends AbstractPersonHelperDto implements UserDetails {

    private String username;

    private String password;

    private Boolean isAccountNonExpired;

    private Boolean isAccountNonLocked;

    private Boolean isCredentialsNonExpired;

    private Boolean isEnabled;





    @Override
    public Collection< ? extends GrantedAuthority > getAuthorities( ) {

        return Collections.singleton( new SimpleGrantedAuthority( "USER" ) );
    }





    @Override
    public String getPassword( ) {

        return password;
    }





    @Override
    public String getUsername( ) {

        return username;
    }





    @Override
    public boolean isAccountNonExpired( ) {

        return isAccountNonExpired;
    }





    @Override
    public boolean isAccountNonLocked( ) {

        return isAccountNonLocked;
    }





    @Override
    public boolean isCredentialsNonExpired( ) {

        return isCredentialsNonExpired;
    }





    @Override
    public boolean isEnabled( ) {

        return isEnabled;
    }

}
