package com.demo.springboot.dto;

import com.demo.springboot.abstrcats.dto.AbstractProfileHelperDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MaterialDto extends AbstractProfileHelperDto {

}
