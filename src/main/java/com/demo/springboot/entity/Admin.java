package com.demo.springboot.entity;

import com.demo.springboot.abstrcats.entity.AbstractPersonHelper;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table( name = "ADMIN" )
public class Admin extends AbstractPersonHelper {

    @Column( name = "USERNAME", nullable = false, length = 20 )
    private String username;

    @Column( name = "PASSWORD", nullable = false, length = 300 )
    private String password;

    @Column( name = "IS_ACCOUNT_NON_EXPIRED", nullable = false )
    private Boolean isAccountNonExpired;

    @Column( name = "IS_ACCOUNT_NON_LOCKED", nullable = false )
    private Boolean isAccountNonLocked;

    @Column( name = "IS_CREDENTIALS_NON_EXPIRED", nullable = false )
    private Boolean isCredentialsNonExpired;

    @Column( name = "IS_ENABLE", nullable = false )
    private Boolean isEnabled;

}
