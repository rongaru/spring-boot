package com.demo.springboot.entity;

import com.demo.springboot.abstrcats.entity.AbstractProfileHelper;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table( name = "MATERIAL" )
public class Material extends AbstractProfileHelper {

}
