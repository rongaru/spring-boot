package com.demo.springboot.mapper;

import com.demo.springboot.abstrcats.mapper.AbstractPersonHelperMapper;
import com.demo.springboot.dto.AdminDto;
import com.demo.springboot.entity.Admin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class AdminMapper extends AbstractPersonHelperMapper {

    public static AdminDto convertToDto( Admin admin ) {

        AdminDto adminDto = new AdminDto( );
        adminDto.setPassword( admin.getPassword( ) );
        adminDto.setUsername( admin.getUsername( ) );
        adminDto.setIsAccountNonExpired( admin.getIsAccountNonExpired( ) );
        adminDto.setIsAccountNonLocked( admin.getIsAccountNonLocked( ) );
        adminDto.setIsCredentialsNonExpired( admin.getIsCredentialsNonExpired( ) );
        adminDto.setIsEnabled( admin.getIsEnabled( ) );
        mapSuperClassProperties( admin, adminDto );
        return adminDto;
    }





    public static List< AdminDto > convertToDtos( List< Admin > admins ) {

        return admins.stream( )
                     .map( AdminMapper :: convertToDto )
                     .collect( Collectors.toList( ) );
    }





    public static Page< AdminDto > convertToDtoPage( Page< Admin > userPage ) {

        return new PageImpl< AdminDto >( convertToDtos( userPage.getContent( ) ),
                userPage.getPageable( ), userPage.getTotalElements( ) );
    }

}
