package com.demo.springboot.mapper;

import com.demo.springboot.abstrcats.mapper.AbstractProfileHelperMapper;
import com.demo.springboot.dto.MaterialDto;
import com.demo.springboot.entity.Material;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class MaterialMapper extends AbstractProfileHelperMapper {

    public static MaterialDto convertToDto( Material material ) {

        MaterialDto materialDto = new MaterialDto( );
        mapSuperClassProperties( material, materialDto );
        return materialDto;
    }





    public static MaterialDto convertToMinimalDto( Material material ) {

        MaterialDto materialDto = new MaterialDto( );
        materialDto.setId( material.getId( ) );
        materialDto.setName( material.getName( ) );
        materialDto.setDescription( material.getDescription( ) );
        return materialDto;
    }





    public static List< MaterialDto > convertToDtos( List< Material > materials ) {

        return materials.stream( )
                        .map( MaterialMapper :: convertToDto )
                        .collect( Collectors.toList( ) );
    }





    public static Page< MaterialDto > convertToDtoPage( Page< Material > rawMaterialPage ) {

        return new PageImpl<>( convertToDtos( rawMaterialPage.getContent( ) ),
                rawMaterialPage.getPageable( ),
                rawMaterialPage.getTotalElements( ) );
    }

}
