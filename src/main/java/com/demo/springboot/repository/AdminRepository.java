package com.demo.springboot.repository;

import com.demo.springboot.abstrcats.repository.AbstractPersonHelperRepository;
import com.demo.springboot.entity.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends AbstractPersonHelperRepository< Admin >, JpaRepository< Admin, Long > {

    Admin findByUsername( String username );

}
