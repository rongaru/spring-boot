package com.demo.springboot.repository;

import com.demo.springboot.abstrcats.repository.AbstractProfileHelperRepository;
import com.demo.springboot.entity.Material;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterialRepository extends AbstractProfileHelperRepository< Material >, JpaRepository< Material, Long > {

}
