package com.demo.springboot.service;

import com.demo.springboot.abstrcats.service.AbstractPersonHelperService;
import com.demo.springboot.dto.AdminDto;

public interface AdminService extends AbstractPersonHelperService< AdminDto > {

}
