package com.demo.springboot.service;

import com.demo.springboot.abstrcats.service.AbstractProfileHelperService;
import com.demo.springboot.dto.MaterialDto;

public interface MaterialService extends AbstractProfileHelperService< MaterialDto > {

}
