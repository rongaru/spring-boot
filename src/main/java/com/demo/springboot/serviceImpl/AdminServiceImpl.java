package com.demo.springboot.serviceImpl;

import com.demo.springboot.Utility.Passwords;
import com.demo.springboot.abstrcats.repository.AbstractPersonHelperRepository;
import com.demo.springboot.abstrcats.serviceImpl.AbstractPersonHelperServiceImpl;
import com.demo.springboot.dto.AdminDto;
import com.demo.springboot.entity.Admin;
import com.demo.springboot.mapper.AdminMapper;
import com.demo.springboot.repository.AdminRepository;
import com.demo.springboot.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AdminServiceImpl extends AbstractPersonHelperServiceImpl< Admin, AdminDto > implements UserDetailsService, AdminService {

    @Autowired
    private AdminRepository adminRepository;





    @Override
    protected AbstractPersonHelperRepository getRepository( ) {

        return adminRepository;
    }





    @Override
    protected AdminRepository getAdminRepository( ) {

        return adminRepository;
    }





    @Override
    protected Admin getEntityInstance( ) {

        return new Admin( );
    }





    @Override
    protected AdminDto convertToDto( Admin user ) {

        return AdminMapper.convertToDto( user );
    }





    @Override
    protected List< AdminDto > convertToDtos( List< Admin > users ) {

        return AdminMapper.convertToDtos( users );
    }





    @Override
    protected Page< AdminDto > convertToDtoPage( Page< Admin > userPage ) {

        return AdminMapper.convertToDtoPage( userPage );
    }





    @Override
    public List< String > getAlreadyExistProperties( AdminDto userDto ) {

        return new ArrayList<>( );
    }





    @Override
    public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException {

        Optional< Admin > user = Optional.of( adminRepository.findByUsername( username ) );
        return convertToDto( user.orElseThrow( ( ) -> new UsernameNotFoundException( "User not found" ) ) );
    }





    @Override
    protected void setCreateEditCommonParameters( Admin user, AdminDto userDto ) {

        user.setAddress( userDto.getAddress( ) );
        user.setDateOfBirth( userDto.getDateOfBirth( ) );
        user.setEmail( userDto.getEmail( ) );
        user.setFirstName( userDto.getFirstName( ) );
        user.setGender( userDto.getGender( ) );
        user.setIsAccountNonExpired( userDto.getIsAccountNonExpired( ) );
        user.setIsAccountNonLocked( userDto.getIsAccountNonLocked( ) );
        user.setIsCredentialsNonExpired( userDto.getIsCredentialsNonExpired( ) );
        user.setIsEnabled( userDto.getIsEnabled( ) );
        user.setLastName( userDto.getLastName( ) );
        user.setPassword( Passwords.encode( userDto.getPassword( ) ) );
        user.setPhone( userDto.getPhone( ) );
        user.setUsername( userDto.getUsername( ) );
    }

}
