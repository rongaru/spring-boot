package com.demo.springboot.serviceImpl;

import com.demo.springboot.Utility.Objects;
import com.demo.springboot.abstrcats.repository.AbstractProfileHelperRepository;
import com.demo.springboot.abstrcats.serviceImpl.AbstractProfileHelperServiceImpl;
import com.demo.springboot.constant.Status;
import com.demo.springboot.dto.MaterialDto;
import com.demo.springboot.entity.Material;
import com.demo.springboot.mapper.MaterialMapper;
import com.demo.springboot.repository.AdminRepository;
import com.demo.springboot.repository.MaterialRepository;
import com.demo.springboot.service.MaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MaterialServiceImpl extends AbstractProfileHelperServiceImpl< Material, MaterialDto > implements MaterialService {

    @Autowired
    private MaterialRepository materialRepository;

    @Autowired
    private AdminRepository adminRepository;





    @Override
    public List< String > getAlreadyExistProperties( MaterialDto materialDto ) {

        List< String > alreadyExistProperties = new ArrayList<>( );
        materialRepository.findByNameAndStatusNotIn( materialDto.getName( ), Status.getDeleteStatuses( ) )
                          .stream( )
                          .filter( material -> Objects.isNotEquals( material, materialDto ) )
                          .findFirst( )
                          .ifPresent( material -> alreadyExistProperties.add( "Name" ) );
        return alreadyExistProperties;
    }





    @Override
    protected AdminRepository getAdminRepository( ) {

        return adminRepository;
    }





    @Override
    protected Material getEntityInstance( ) {

        return new Material( );
    }





    @Override
    protected void setCreateEditCommonParameters( Material material, MaterialDto materialDto ) {

        material.setDescription( materialDto.getDescription( ) );
        material.setName( materialDto.getName( ) );
    }





    @Override
    protected MaterialDto convertToDto( Material material ) {

        return MaterialMapper.convertToDto( material );
    }





    @Override
    protected List< MaterialDto > convertToDtos( List< Material > materials ) {

        return MaterialMapper.convertToDtos( materials );
    }





    @Override
    protected Page< MaterialDto > convertToDtoPage( Page< Material > rawMaterialPage ) {

        return MaterialMapper.convertToDtoPage( rawMaterialPage );
    }





    @Override
    protected AbstractProfileHelperRepository getRepository( ) {

        return materialRepository;
    }

}
