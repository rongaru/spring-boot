//create edit panel handler
$(document).ready(function () {
    // $("body").toggleClass("sb-sidenav-toggled");
    $('#createEditPanel').modal('show');
});

//message handler
$(document).ready(function () {
    if (messageType.length > 0 && messageTitle.length > 0) {
        notify(messageType, messageTitle, messageDetail);
    }
});

//cancel button handler
$(document).ready(function () {
    $('#btnCancel').on("click", () => $('#createEditPanel').modal('hide'));
});

//pagination handler
$(document).ready(function () {
    var paginations = $('pagination');
    $(paginations).each(function (index) {
        var pagination = paginations[index];
        var url = pagination.getAttribute("url");
        var active = parseInt(pagination.getAttribute("active")) + 1;
        var size = parseInt(pagination.getAttribute("size"));
        var total = parseInt(pagination.getAttribute("total"));

        //paginating
        $(pagination).append(createButton('First', 'page', active === 1, paginationLink(url, 1, size)));
        $(pagination).append(createButton('Prev', 'page', active === 1, paginationLink(url, active - 1, size)));
        for (var i = 1; i <= total; i++) {
            var page = paginationLink(url, i, size);
            $(pagination).append(createButton(i, 'page', i === active, page));
        }
        $(pagination).append(createButton('Next', 'page', active == total, paginationLink(url, active + 1, size)));
        $(pagination).append(createButton('Last', 'page', active == total, paginationLink(url, total, size)));
    });

    //create pagination button
    function createButton(text, styleClass, disabled, href) {
        var button = document.createElement("button");
        button.innerText = text;
        button.className = styleClass;
        button.disabled = disabled;
        button.onclick = () => location.replace(button.getAttribute('href'));
        button.setAttribute("href", href);
        return button;
    }

    //create pagination link
    function paginationLink(url, pageNumber, size) {
        var link = url + '?pageNumber=' + pageNumber + '&pageSize=' + size;
        return link;
    }
});
